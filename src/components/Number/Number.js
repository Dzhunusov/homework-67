import React from 'react';
import './Number.css';

const Number = props => {
  return (
      <button>
        {props.number}
      </button>
  );
};

export default Number;