import React, {useState} from 'react';
import {useSelector, useDispatch} from "react-redux";
import './Calculator.css';
import Number from "../../components/Number/Number";

const Calculator = () => {
  const num = useSelector(state => state.number);
  const dispatch = useDispatch();

  const modifyNum = value => dispatch({type: 'MODIFY', value});

  return (
      <div className="calculator">
        <div className="enter-block">
          {num}
        </div>
        <div className="calculator-btns">
          <button onClick={() => modifyNum(1)}>1</button>
          <button onClick={() => modifyNum(2)}>2</button>
          <button onClick={() => modifyNum(3)}>3</button>
          <button onClick={() => modifyNum(4)}>4</button>
          <button onClick={() => modifyNum(5)}>5</button>
          <button onClick={() => modifyNum(6)}>6</button>
          <button onClick={() => modifyNum(7)}>7</button>
          <button onClick={() => modifyNum(8)}>8</button>
          <button onClick={() => modifyNum(9)}>9</button>
          <button onClick={() => modifyNum(0)}>0</button>
          <button>+</button>
          <button>-</button>
          <button>/</button>
          <button>*</button>
          <button>=</button>
        </div>
      </div>
  );
};

export default Calculator;